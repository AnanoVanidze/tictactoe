package com.example.tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.widget.Button
import android.widget.Toast
import com.example.tictactoe1.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var firstPlayer = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button00.setOnClickListener {
            aboutButton(button00)


        }
        button01.setOnClickListener {
            aboutButton(button01)



        }
        button02.setOnClickListener {
            aboutButton(button02)


        }
        button10.setOnClickListener {
            aboutButton(button10)


        }
        button11.setOnClickListener {
            aboutButton(button11)


        }
        button12.setOnClickListener {
            aboutButton(button12)


        }
        button20.setOnClickListener {
            aboutButton(button20)


        }
        button21.setOnClickListener {
            aboutButton(button21)


        }
        button22.setOnClickListener {
            aboutButton(button22)


        }
        tryAgainButton.setOnClickListener {
            resetButton()
            reset()

        }

    }
    private fun  resetButton() {
        button00.text = ""
        button01.text = ""
        button02.text = ""
        button10.text = ""
        button11.text = ""
        button12.text = ""
        button20.text = ""
        button21.text = ""
        button22.text = ""
    }
    private fun reset() {
        button00.isClickable = true
        button01.isClickable = true
        button02.isClickable = true
        button10.isClickable = true
        button11.isClickable = true
        button12.isClickable = true
        button20.isClickable = true
        button21.isClickable = true
        button22.isClickable = true
    }

    private fun aboutButton(button: Button) {
        if (firstPlayer) {
            button.text = "x"
        } else {
            button.text = "0"
        }
        button.isClickable = false
        firstPlayer = !firstPlayer
        chekWinner()



    }
    private fun chekWinner() {
        if (button00.text.toString().isNotEmpty() && button00.text.toString() == button01.text.toString() && button00.text.toString() == button02.text.toString())
            showToast(button00.text.toString())
        else if (button10.text.toString().isNotEmpty() && button10.text.toString() == button11.text.toString() && button10.text.toString() == button12.text.toString())
            showToast(button10.text.toString())
        else if (button20.text.toString().isNotEmpty() && button20.text.toString() == button21.text.toString() && button20.text.toString() == button22.text.toString())
            showToast(button20.text.toString())

        else if (button00.text.toString().isNotEmpty() && button00.text.toString() == button10.text.toString() && button00.text.toString() == button20.text.toString())
            showToast(button00.text.toString())

        else if (button01.text.toString().isNotEmpty() && button01.text.toString() == button11.text.toString() && button01.text.toString() == button21.text.toString())
            showToast(button01.text.toString())
        else if (button02.text.toString()
                .isNotEmpty() && button02.text.toString() == button12.text.toString() && button02.text.toString() == button22.text.toString())
            showToast(button02.text.toString())
        else if (button00.text.toString()
                .isNotEmpty() && button00.text.toString() == button11.text.toString() && button00.text.toString() == button22.text.toString())
            showToast(button00.text.toString())
        else if (button02.text.toString()
                .isNotEmpty() && button02.text.toString() == button11.text.toString() && button02.text.toString() == button20.text.toString())
            showToast(button02.text.toString())
        else if (button00.text.toString()
                .isNotEmpty() && button01.text.isNotEmpty() && button02.text.isNotEmpty() && button00.text.toString() != button01.text.toString() && button00.text.toString() != button02.text.toString()
        )
            showDraw()
        else if (button10.text.toString()
                .isNotEmpty() && button11.text.isNotEmpty() && button12.text.isNotEmpty() && button10.text.toString() != button11.text.toString() && button10.text.toString() != button12.text.toString()
        )
            showDraw()
        else if (button20.text.toString()
                .isNotEmpty() && button21.text.isNotEmpty() && button22.text.isNotEmpty() && button20.text.toString() != button21.text.toString() && button20.text.toString() != button22.text.toString()
        )
            showDraw()
        else if (button00.text.toString()
                .isNotEmpty() && button10.text.isNotEmpty() && button20.text.isNotEmpty() && button00.text.toString() != button10.text.toString() && button00.text.toString() != button20.text.toString()
        )
            showDraw()
        else if (button01.text.toString()
                .isNotEmpty() && button11.text.isNotEmpty() && button02.text.isNotEmpty() && button21.text.toString() != button11.text.toString() && button01.text.toString() != button21.text.toString()
        )
            showDraw()
        else if (button02.text.toString()
                .isNotEmpty() && button12.text.isNotEmpty() && button22.text.isNotEmpty() && button02.text.toString() != button12.text.toString() && button02.text.toString() != button22.text.toString()
        )
            showDraw()
        else if (button00.text.toString()
                .isNotEmpty() && button11.text.isNotEmpty() && button22.text.isNotEmpty() && button00.text.toString() != button11.text.toString() && button00.text.toString() != button22.text.toString()
        )
            showDraw()
        else if (button02.text.toString()
                .isNotEmpty() && button11.text.isNotEmpty() && button20.text.isNotEmpty() && button02.text.toString() != button11.text.toString() && button02.text.toString() != button20.text.toString()
        )
            showDraw()


    }

    private fun showToast(message: String) {
        Toast.makeText(this, "The Winner is ${message}", Toast.LENGTH_SHORT).show()

    }
    private fun showDraw() {
        Toast.makeText(this, "Draw", Toast.LENGTH_SHORT).show()

    }







}
